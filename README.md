INTRODUCTION
------------
Disallow Robotstxt is  module that destroys the fun and joy of robotstxt by
disallowing all pages on the site with a checkbox or with a server check.


REQUIREMENTS
------------
This module requires robotstxt


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure Disallow Robotstxt at Administration >> Configuration >> Search >>
   Disallow Robotstxt Settings


FAQ
---
Q: Does the Disallow Robots.txt destroy my settings?

A: Nope, just allows for you to sites on the internet without having search
   engines being creepy crawlers.


Q: Should I use this on a beta site?

A: Most definitely!

Q: Are there any side effects of this module?

A: You might forget to turn it off when you go live, that could be bad, but easy
   to remedy.

MAINTAINERS
-----------
Current maintainers:
 * Tim Whitney (timodwhit) - https://www.drupal.org/user/1629156


This project has been sponsored by:
 * Miles
 Visit https://milespartnership.com
